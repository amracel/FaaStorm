package edu.hu.faa.storm.spout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

/**
 * Send requests to FAA web service. Get JSON string into stream.
 * @author anne
 */
public class AirportStatusSpout extends BaseRichSpout {

	private static final long serialVersionUID = 5495969177523635158L;
	SpoutOutputCollector _collector;
	private static int SLEEP = 1000;
	private List<String> airportCodes = new ArrayList<String>();
	private final String USER_AGENT = "Mozilla/5.0";
	private int currentCode = 0;

        /**
         * Set collector. Set airport codes.
         * @param conf
         * @param context
         * @param collector 
         */
	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		_collector = collector;
		airportCodes.add("SFO");
		airportCodes.add("BWI");
		airportCodes.add("MIA");
		airportCodes.add("ORD");
		airportCodes.add("ATL");
		airportCodes.add("LAX");
		airportCodes.add("BOS");
		airportCodes.add("STL");
		airportCodes.add("DFW");
		airportCodes.add("DEN");
		airportCodes.add("SEA");
		airportCodes.add("CLT");
	}

        /**
         * Send request. Collect JSON if request is successful.
         */
	@Override
	public void nextTuple() {
		Utils.sleep(SLEEP);
		String code = airportCodes.get(getCurrentCode());
		String url = "http://services.faa.gov/airport/status/" + code
				+ "?format=application/json";
		StringBuffer nextStatus = new StringBuffer();
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();

			if (responseCode == 200) {

				BufferedReader br = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String tmp;
				while ((tmp = br.readLine()) != null) {
					nextStatus.append(tmp);
				}
				_collector.emit(new Values(nextStatus.toString()));

			} else {
				Utils.sleep(SLEEP);
			}
		} catch (MalformedURLException mx) {

		} catch (IOException ioex) {

		}

	}

        /**
         * Give name to the new output field.
         * @param declarer 
         */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("status"));

	}

        /**
         * Not handled.
         * @param id 
         */
	@Override
	public void fail(Object id) {

	}

        /**
         * Cycle through the set of codes. Return next int. If we've reached
         * the end of the list, start again at the beginning.
         * @return 
         */
	private int getCurrentCode() {
		if (currentCode == (airportCodes.size() - 1))
			currentCode = 0;
		else
			++currentCode;
		return currentCode;
	}

}
