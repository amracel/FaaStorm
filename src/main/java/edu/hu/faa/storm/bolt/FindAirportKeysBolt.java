package edu.hu.faa.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import edu.hu.faa.storm.pojos.AirportStatus;
import java.util.Map;

/**
 * Create new records. Key is the IATA (airport) code. If there's a delay, the
 * value is '1'.
 * @author Anne Racel amracel@stuffandjunk.net
 */
public class FindAirportKeysBolt extends BaseRichBolt {
    
    OutputCollector _collector = null;

    /**
     * Set up names of the new output fields.
     * @param declarer 
     */
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("delay-counter","delay-count"));
    }

    /**
     * Initialize collector.
     * @param stormConf
     * @param context
     * @param collector 
     */
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        _collector = collector;
    }

    /**
     * Create new record. Key is IATA (airport) code. Value is '1' if there's a 
     * delay, '0' if no delay.
     * @param input 
     */
    @Override
    public void execute(Tuple input) {
            AirportStatus status = (AirportStatus)input.getValue(0);
            if(status.isDelay()) {
                _collector.emit(new Values(status.getIata(),1));
            }
            else {
                _collector.emit(new Values(status.getIata(), 0));
            }
    }
    
}
