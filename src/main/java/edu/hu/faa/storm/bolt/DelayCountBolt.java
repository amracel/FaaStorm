package edu.hu.faa.storm.bolt;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import edu.hu.faa.storm.pojos.AirportStatus;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Increment counters for airports with delays.
 * @author Anne Racel <amracel@stuffandjunk.net>
 */
public class DelayCountBolt extends BaseRichBolt {
    
    Map<String, Integer> counters;
    private OutputCollector _collector;
    private static final Logger LOG = Logger.getLogger(DelayCountBolt.class);

    /**
     * No data is passed from here, so this method is blank.
     * @param declarer 
     */
    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        // nothing needed here?
    }

    /**
     * Setup
     * @param stormConf
     * @param context
     * @param collector 
     */
    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.counters = new HashMap<String,Integer>();
        _collector = collector;
    }

    /**
     * Take in a record. If there's a delay, increment a counter and print a
     * record.
     * @param input 
     */
    @Override
    public void execute(Tuple input) {
        AirportStatus status = (AirportStatus) input.getValue(0);
        LOG.info(status.getIata());
        if(!counters.containsKey(status.getIata()))
            counters.put(status.getIata(), 0);
        if(status.isDelay()) {
            Integer currCount = counters.get(status.getIata()) + 1;
            counters.put(status.getIata(), currCount);
            printStatus();
        }
        
        _collector.ack(input);
    }
    /**
     * Print the status of all the airports.
     */
    public void printStatus() {
        for(Map.Entry<String,Integer> entry: counters.entrySet()) {
            LOG.info("+++" + entry.getKey() + ": " + entry.getValue() + "+++");
        }
    }
}
