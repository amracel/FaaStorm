package edu.hu.faa.storm.bolt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import edu.hu.faa.storm.pojos.AirportStatus;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * Read and process the incoming JSON string.
 * @author anne
 */
public class ProcessJsonBolt extends BaseRichBolt {

	private static final long serialVersionUID = -216993006091862955L;
	private static final Logger LOG = Logger.getLogger(ProcessJsonBolt.class);
	private AirportStatus airportStatus = null;
	private OutputCollector _collector = null;

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;

	}

        /**
         * Convert JSON string to AirportStatus object.
         * @param input JSON string from FAA web service.
         */
	@Override
	public void execute(Tuple input) {
                airportStatus = new AirportStatus();
		String jsonString = input.getString(0);
		JSONParser parser = new JSONParser();
		List<Object> statuses = new ArrayList<Object>();
		try {
			JSONObject jsonObject = (JSONObject) parser.parse(jsonString);
			JSONObject weatherObject = (JSONObject) jsonObject.get("weather");
                        Logger.getLogger(ProcessJsonBolt.class).debug("weather:" + weatherObject.toString());
			JSONObject statusObject = (JSONObject) jsonObject.get("status");
                        airportStatus.setIata(jsonObject.get("IATA").toString());
                        airportStatus.setDelay(jsonObject.get("delay").toString());
                        if(weatherObject.get("temp") != null)
                            airportStatus.setTemp(weatherObject.get("temp").toString());
                        if(weatherObject.get("wind") != null)
                            airportStatus.setWind(weatherObject.get("wind").toString());
			String avgDelay = statusObject.get("avgDelay").toString();
			if (!avgDelay.isEmpty()) {
				airportStatus.setAvgDelay(avgDelay);
				airportStatus.setDelay(jsonObject.get("delay").toString());
				airportStatus.setReason(statusObject.get("reason").toString());
			}
			statuses.add(airportStatus);
			_collector.emit(statuses);
			_collector.ack(input);
		} catch (ParseException pe) {
			LOG.error(pe.getMessage());
			_collector.fail(input);
		}
	}

        /**
         * Give a name to the outgoing record.
         * @param declarer 
         */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("airportstatus"));

	}
        /**
         * Get current logger.
         * @return 
         */
	  Logger getLogger() {
	    return LOG;
	  }

}
