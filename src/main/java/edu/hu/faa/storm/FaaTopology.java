package edu.hu.faa.storm;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import edu.hu.faa.storm.bolt.DelayCountBolt;
import edu.hu.faa.storm.bolt.FindAirportKeysBolt;
import edu.hu.faa.storm.bolt.ProcessJsonBolt;
import edu.hu.faa.storm.bolt.RollingCountBolt;
import edu.hu.faa.storm.spout.AirportStatusSpout;
import org.apache.log4j.Logger;

/**
 * Controller for program. Creates connections between spouts (data sources)
 * and bolts (data manipulations).
 * Expects Apache Storm to be installed to run. 
 * See: https://storm.apache.org/documentation/Setting-up-development-environment.html
 * Usage: storm jar FaaStorm-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
 * edu.hu.faa.storm.FaaTopology
 * @author Anne Racel amracel@stuffandjunk.net
 */
public class FaaTopology {
    
    private static final Logger LOG = Logger.getLogger(FaaTopology.class);
    private static final int DEFAULT_RUNTIME_IN_SECONDS = 60;
    private static final int TOP_N = 15;
    
    private final TopologyBuilder builder;
    private final String topologyName;
    private final Config topologyConfig;
    private final int runtimeInSeconds;
    
    /**
     * Initialize variables call for wiring of topology.
     * @param topologyName name to use for this topology
     * @throws InterruptedException 
     */
    public FaaTopology(String topologyName) throws InterruptedException {
        builder = new TopologyBuilder();
        this.topologyName = topologyName;
        topologyConfig = createTopologyConfiguration();
        runtimeInSeconds = DEFAULT_RUNTIME_IN_SECONDS;
        
        wireTopology();
    }
    
    /**
     * Generic configuration. Sets debugging as 'true'.
     * @return 
     */
    private static Config createTopologyConfiguration() {
        Config config = new Config();
        config.setDebug(true);
        return config;
    }
    /**
     * Wire data sources (spouts) and bolts (data processing).
     */
    private void wireTopology() {
        builder.setSpout("read-status", new AirportStatusSpout());
        builder.setBolt("process-json", new ProcessJsonBolt()).shuffleGrouping("read-status");
        builder.setBolt("delays", new DelayCountBolt()).shuffleGrouping("process-json");
        
    }
    
    /**
     * Start running.
     */
    public void runTopology() {
        Config conf = createTopologyConfiguration();
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("faa", conf, builder.createTopology());
    }

    public static void main(String[] args) throws Exception {

        FaaTopology topology = new FaaTopology("Flight Delays");
        topology.runTopology();
          
    }
}
